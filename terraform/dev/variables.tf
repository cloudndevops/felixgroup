variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AMIS" {
   type = map(string)
   default = {
    ap-southeast-1 = "ami-05c64f7b4062b0a21"
  }

}
variable "AWS_REGION" {
  default = "ap-southeast-1"
}
variable "AWS_SB_REGION" {
  type = map(string)
  default = {
   av-a = "ap-southeast-1a"
   av-b = "ap-southeast-1b"
   av-c = "ap-southeast-1c"
 }
}
variable "alarm_cpu_threshold" { default = 75 }
variable "alarm_disk_queue_threshold" { default = 10 }
variable "multiaz" { default = true }
variable "skip_final_snapshot" { default = true }
variable "final_snapshot_identifier" { default = "" }
