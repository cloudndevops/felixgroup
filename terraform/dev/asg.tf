resource "aws_launch_configuration" "wp-launch-config" {
  name_prefix          = "wp-launch-config"
  image_id             = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type        = "t2.micro"
  key_name             = "${aws_key_pair.wpkey.key_name}"
  user_data            = file("../scripts/bootstrap.sh")
  security_groups      = ["${aws_security_group.wp-sg.id}"]
}

resource "aws_autoscaling_group" "wp-asg" {
  name                 = "wp-asg"
  vpc_zone_identifier  = ["${aws_subnet.wp-subnet-private-1.id}"]
  launch_configuration = "${aws_launch_configuration.wp-launch-config.name}"
  min_size             = 0
  max_size             = 4
  health_check_grace_period = 300
  health_check_type = "EC2"
  force_delete = true

  tag {
      key = "Name"
      value = "ec2 instance"
      propagate_at_launch = true
  }
}
# scale up alarm

resource "aws_autoscaling_policy" "wp-cpu-policy" {
  name                   = "wp-cpu-policy"
  autoscaling_group_name = "${aws_autoscaling_group.wp-asg.name}"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "wp-cpu-alarm" {
  alarm_name          = "wp-cpu-alarm"
  alarm_description   = "wp-cpu-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "30"

  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.wp-asg.name}"
  }

  actions_enabled = true
  alarm_actions   = ["${aws_autoscaling_policy.wp-cpu-policy.arn}"]
}

# scale down alarm
resource "aws_autoscaling_policy" "wp-cpu-policy-scaledown" {
  name                   = "wp-cpu-policy-scaledown"
  autoscaling_group_name = "${aws_autoscaling_group.wp-asg.name}"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "-1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "wp-cpu-alarm-scaledown" {
  alarm_name          = "wp-cpu-alarm-scaledown"
  alarm_description   = "wp-cpu-alarm-scaledown"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "5"

  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.wp-asg.name}"
  }

  actions_enabled = true
  alarm_actions   = ["${aws_autoscaling_policy.wp-cpu-policy-scaledown.arn}"]
}

