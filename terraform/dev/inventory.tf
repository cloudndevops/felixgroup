data  "template_file" "inventory" {
    template = file("ansible.tpl")
    vars = {
        appserverip1 = "${join("\n", aws_instance.appserver1.*.private_ip)}"
        appserverip2 = "${join("\n", aws_instance.appserver2.*.private_ip)}"
        dbendpoint = "${aws_db_instance.mysqlrds.endpoint}"
        wlbdns = "${aws_lb.wp-wlb.dns_name}"
    }
}
resource "local_file" "ansible_inventory" {
  content  = "${data.template_file.inventory.rendered}"
  filename = "../playbooks/hosts"
}
