resource "aws_db_instance" "mysqlrds" {
  engine         	     = "mysql"
  engine_version 	     = "5.7.22"
  instance_class             = "db.t2.micro"
  storage_type               = "gp2"
  storage_encrypted          = "false"
  allocated_storage          = "20"
  name                       = "wordpress"
  username                   = "admin"
  password                   = "admin123"
  port                       = "3306"
  multi_az                   = "false"

  tags = {
    Name                     = "wpsql"
  }

  timeouts {
    create = "60m"
    update = "60m"
    delete = "60m"
  }
}

resource "aws_security_group" "rds-sg" {
  name        = "rds-sg"
  description = "Ingress from efresh to instances"
  vpc_id      = "${aws_vpc.wp-vpc.id}"

  tags = {
    Name                     = "rds-sg"
    Type                     = "EC2 Security Group"
    Monitoring               = "true"
  }
}
