resource "aws_lb" "wp-wlb" {
  name               = "wp-wlb"
  internal           = false
  load_balancer_type = "application"
  subnets            = ["${aws_subnet.wp-subnet-private-1.id}", "${aws_subnet.wp-subnet-private-2.id}"]
  security_groups    = ["${aws_security_group.wp-sg.id}"]

  tags = {
    Name = "wp-wlb"
  }
}
resource "aws_lb_target_group" "wlb-to-ip" {
  name        = "wlb-to-ip"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${aws_vpc.wp-vpc.id}"
}

resource "aws_lb_target_group_attachment" "wlb-attach-target-1" {
  count            = 1
  target_group_arn = "${aws_lb_target_group.wlb-to-ip.arn}"
  target_id        = "${aws_instance.appserver1[count.index].id}"
  port             = 80
}

resource "aws_lb_target_group_attachment" "wlb-attach-target-2" {
  count            = 1
  target_group_arn = "${aws_lb_target_group.wlb-to-ip.arn}"
  target_id        = "${aws_instance.appserver2[count.index].id}"
  port             = 80
}

resource "aws_lb_listener" "wp-wlb-listener" {
  load_balancer_arn = "${aws_lb.wp-wlb.arn}"
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.wlb-to-ip.arn}"
  }
}
