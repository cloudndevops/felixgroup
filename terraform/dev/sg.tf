resource "aws_security_group" "wp-sg" {
  name        = "wp-sg"
  description = "Ingress from efresh to instances"
  vpc_id      = "${aws_vpc.wp-vpc.id}"

  tags = {
    Name                     = "wp-sg"
    Type                     = "EC2 Security Group"
    Monitoring               = "true"
  }
}
