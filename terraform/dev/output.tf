output "appserver1-ip" {
  value = "${aws_instance.appserver1.*.private_ip}"
}
output "appserver2-ip" {
  value = "${aws_instance.appserver2.*.private_ip}"
}
output "baston" {
  value = "${aws_instance.baston.*.public_ip}"
}
output "db_endpoint" {
  value = "${aws_db_instance.mysqlrds.endpoint}"
}
output "wlb" {
  value = "${aws_lb.wp-wlb.dns_name}"
}
