# Internet VPC
resource "aws_vpc" "wp-vpc" {
    cidr_block = "10.0.0.0/16"
    instance_tenancy = "default"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
    enable_classiclink = "false"
    tags = {
        Name = "wp-vpc"
    }
}


# Subnets
#********

resource "aws_subnet" "wp-subnet-private-1" {
    vpc_id = "${aws_vpc.wp-vpc.id}"
    cidr_block = "10.0.1.0/24"
    availability_zone = "${var.AWS_SB_REGION.av-a}"

    tags = {
        Name = "wp-subnet-private-1"
    }
}

resource "aws_subnet" "wp-subnet-private-2" {
    vpc_id = "${aws_vpc.wp-vpc.id}"
    cidr_block = "10.0.2.0/24"
    availability_zone = "${var.AWS_SB_REGION.av-b}"

    tags = {
        Name = "wp-subnet-private-2"
    }
}


resource "aws_subnet" "wp-subnet-public-1" {
    vpc_id = "${aws_vpc.wp-vpc.id}"
    cidr_block = "10.0.3.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "${var.AWS_SB_REGION.av-a}"

    tags = {
        Name = "wp-subnet-public-1"
    }
}

resource "aws_subnet" "wp-subnet-public-2" {
    vpc_id = "${aws_vpc.wp-vpc.id}"
    cidr_block = "10.0.4.0/24"
    map_public_ip_on_launch = "false"
    availability_zone = "${var.AWS_SB_REGION.av-b}"

    tags = {
        Name = "wp-subnet-public-2"
    }
}


# NAT Gateway
#**************

resource "aws_eip" "wp-nat-1" {
  vpc      = true
}
resource "aws_nat_gateway" "wp-nat-gw-1" {
  allocation_id = "${aws_eip.wp-nat-1.id}"
  subnet_id = "${aws_subnet.wp-subnet-public-1.id}"
  depends_on = [aws_internet_gateway.wp-gw]
}



resource "aws_eip" "wp-nat-2" {
  vpc      = true
}
resource "aws_nat_gateway" "wp-nat-gw-2" {
  allocation_id = "${aws_eip.wp-nat-2.id}"
  subnet_id = "${aws_subnet.wp-subnet-public-2.id}"
  depends_on = [aws_internet_gateway.wp-gw]
}



# Network ACL's
#**************

resource "aws_network_acl" "wp-vpc" {
  vpc_id = aws_vpc.wp-vpc.id

  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "10.0.3.0/24"
    from_port  = 443
    to_port    = 443
  }

  egress {
    protocol   = "tcp"
    rule_no    = 110
    action     = "allow"
    cidr_block = "10.0.4.0/24"
    from_port  = 443
    to_port    = 443
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = "10.0.3.0/24"
    from_port  = 80
    to_port    = 80
  }

  ingress {
    protocol   = "tcp"
    rule_no    = 210
    action     = "allow"
    cidr_block = "10.0.4.0/24"
    from_port  = 80
    to_port    = 80
  }


  tags = {
    Name = "wp-vpc"
  }
}


# Route tables
#*************
resource "aws_route_table" "wp-route-private" {
    vpc_id = "${aws_vpc.wp-vpc.id}"
    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.wp-gw.id}"
    }

    tags = {
        Name = "wp-route-private"
    }
}

resource "aws_route_table" "wp-route-public" {
    vpc_id = "${aws_vpc.wp-vpc.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.wp-gw.id}"
    }

    tags = {
        Name = "wp-route-public"
    }
}


# Route table associations 
#*************************#

# Private
#********

resource "aws_route_table_association" "wp-route-private-1-assoc" {
    subnet_id = "${aws_subnet.wp-subnet-private-1.id}"
    route_table_id = "${aws_route_table.wp-route-private.id}"
}
resource "aws_route_table_association" "wp-route-private-2-assoc" {
    subnet_id = "${aws_subnet.wp-subnet-private-2.id}"
    route_table_id = "${aws_route_table.wp-route-private.id}"
}

# Public
#********

resource "aws_route_table_association" "wp-route-public-1-assoc" {
    subnet_id = "${aws_subnet.wp-subnet-public-1.id}"
    route_table_id = "${aws_route_table.wp-route-public.id}"
}

resource "aws_route_table_association" "wp-route-public-2-assoc" {
    subnet_id = "${aws_subnet.wp-subnet-public-2.id}"
    route_table_id = "${aws_route_table.wp-route-public.id}"
}

# Internet GW
#************

resource "aws_internet_gateway" "wp-gw" {
    vpc_id = "${aws_vpc.wp-vpc.id}"

    tags = {
        Name = "wp-gw"
    }
}

